# srrg_kobuki_ros

This package contains a minimalistic driver for the kobuki robot
(aka **turtlebot 2**). It is built from the serial protocol specifications,
and it does not use the on-board gyro. It is muuuuch simpler than the
official node.

## Setting up
Just copy the node in your ros_package_path and compile

Requires `srrg_core` for the ticks message.

Parameters:
* `_serial_device`: (/dev/ttyUSB0) the serial port
* `_odom_topic`: (/odom) the odometry topic 
* `_command_vel_topic`: (/cmd_vel) the cmd_vel topic
* `_joint_state_topic`: (/joint_state) joint state topic for use with calibration tools
* `_ticks_topic`: (/ticks) raw encoder ticks message
* `_odom_frame_id`: (/odom) frame id for the odometry message

That's all.

## Authors

* **Giorgio Grisetti**

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

BSD 2.0
