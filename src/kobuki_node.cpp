#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <srrg_core_ros/Ticks.h>
#include "srrg_kobuki_library/kobuki_robot.h"
#include <iostream>
#include<tf/tf.h>
using namespace std;
using namespace srrg_kobuki;

KobukiRobot robot;
volatile double tv = 0, rv = 0;
const double uint16_to_radians = 2*M_PI/65536;

void commandVelCallback(const geometry_msgs::TwistConstPtr twist){
  tv = twist->linear.x;
  rv = twist->angular.z;
}

int main(int argc, char** argv) {
  std::string serial_device;
  std::string odom_topic;
  std::string ticks_topic;
  std::string odom_frame_id;
  std::string command_vel_topic;
  std::string joint_state_topic;

  ros::init(argc, argv, "free_kobuki_node");
  ros::NodeHandle nh("~");
  nh.param("serial_device", serial_device, std::string("/dev/ttyUSB0"));
  nh.param("odom_topic", odom_topic, std::string("/odom"));
  nh.param("ticks_topic", ticks_topic, std::string("/ticks"));
  nh.param("joint_state_topic", joint_state_topic, std::string("/joint_state"));
  nh.param("command_vel_topic", command_vel_topic, std::string("/cmd_vel"));
  nh.param("odom_frame_id", odom_frame_id, std::string("/odom"));



  cerr << "running with params: ";
  cerr << "serial_device: " << serial_device << endl;
  cerr << "odom_topic: " << odom_topic << endl;
  cerr << "command_vel_topic: " << command_vel_topic << endl;
  cerr << "joint_state_topic: " << joint_state_topic << endl;
  cerr << "ticks_topic: " << ticks_topic << endl;
  cerr << "odom_frame_id: " << odom_frame_id << endl;

  ros::Subscriber command_vel_subscriber = nh.subscribe<geometry_msgs::TwistConstPtr>(command_vel_topic, 1, &commandVelCallback);
  ros::Publisher odom_publisher = nh.advertise<nav_msgs::Odometry>(odom_topic, 1);
  ros::Publisher ticks_publisher = nh.advertise<srrg_core_ros::Ticks>(ticks_topic, 1);
  ros::Publisher joint_state_publisher = nh.advertise<sensor_msgs::JointState>(joint_state_topic, 1);

  robot.connect(serial_device);
  ros::Rate r(100);
  nav_msgs::Odometry odom;
  srrg_core_ros::Ticks ticks;
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(2);
  joint_state.position.resize(2);
  joint_state.velocity.resize(2);
  joint_state.effort.resize(2);

  joint_state.name[0]="left_wheel";
  joint_state.name[1]="right_wheel";
  joint_state.effort[0]=0;
  joint_state.effort[1]=0;

  odom.header.frame_id = odom_frame_id;
  int seq = 0;
  int packet_count = 0;
  while(ros::ok()){
    ros::spinOnce();
    robot.spinOnce();
    if (packet_count < robot.packetCount()) {
      // send the odometry
      double x,y,theta;
      robot.getOdometry(x,y,theta);
      odom.header.seq = seq;
      odom.header.stamp = ros::Time::now();
      odom.pose.pose.position.x = x;
      odom.pose.pose.position.y = y;
      odom.pose.pose.position.z = 0;
      /*double s = sin (theta/2);
      double c = cos (theta/2);
      odom.pose.pose.orientation.x = 0;
      odom.pose.pose.orientation.y = 0;
      odom.pose.pose.orientation.z = s;
      odom.pose.pose.orientation.w = c;*/
      odom.pose.pose.orientation=tf::createQuaternionMsgFromYaw(theta);
      odom_publisher.publish(odom);
      uint16_t left_ticks, right_ticks;
      robot.getTicks(left_ticks,right_ticks);

      ticks.header = odom.header;
      ticks.leftEncoder=left_ticks;
      ticks.rightEncoder=right_ticks;
      ticks_publisher.publish(ticks);

      joint_state.header=odom.header;
      
      joint_state.position[0]=uint16_to_radians*left_ticks;
      joint_state.position[1]=uint16_to_radians*right_ticks;
      joint_state.velocity[0]=0;
      joint_state.velocity[1]=0;
      joint_state_publisher.publish(joint_state);

      seq++;
      packet_count = robot.packetCount();
      robot.setSpeed(tv, rv);
    }
    r.sleep();
  }
}
